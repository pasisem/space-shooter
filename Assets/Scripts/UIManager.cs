﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private GameManager _gm;
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private Text _continueText;
    [SerializeField]
    private Text _gameOverText;
    [SerializeField]
    private Image _livesImage;
    [SerializeField]
    private Sprite[] _livesSpires;

    // Start is called before the first frame update
    void Start()
    {
        if (_gm == null)
            _gm = GameObject.Find("Game_Manager").GetComponent<GameManager>();


        if (_gm == null)
            Debug.LogError("UIManager failed to find Game Manager.");

        _scoreText.text = "Kills: 0";

#if UNITY_ANDROID
        _continueText.text = "Tap screen to restart";
#else
        _continueText.text = "Press R to restart";
#endif
    }

    // Update is called once per frame
    void Update()
    {
    }


    public void ShowGameOverText()
    {
        _gm.GameOver();
        _gameOverText.gameObject.SetActive(true);
        _gameOverText.enabled = true;
        StartCoroutine(GameOverTextBuilding());
    }

    public void UpdateScore(int score)
    {
        _scoreText.text = "Kills: "+score;
    }


    public void UpdateLives(int lives)
    {
        if (lives > -1 && lives < _livesSpires.Length)
            _livesImage.sprite = _livesSpires[lives];
        else
            Debug.LogError("Updated lives out of sprite set scope.");
    }


    IEnumerator FlickerGameOverText()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            _gameOverText.enabled = !_gameOverText.enabled;

        }
    }
    IEnumerator GameOverTextBuilding()
    {
        float letterDelay = 0.03f;
        string text = "GAME OVER";
        while (true)
        {
            _gameOverText.text = "";

            foreach (char c in text)
            {
                _gameOverText.text += c;

                yield return new WaitForSeconds(letterDelay);
            }

            for (int i = 0; i < 6; i++)
            {
                yield return new WaitForSeconds(0.5f);
                _gameOverText.enabled = !_gameOverText.enabled;
            }
        }
    }

}
