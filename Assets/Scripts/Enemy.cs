﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float _speed = 4f;
    private Player _player;
    private bool _isDead = false;

    void Update()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        if (transform.position.y > 7f 
            || transform.position.x < -10f
            || transform.position.x > 10f)
            Destroy(this.gameObject);
        else if(transform.position.y < -7f && !_isDead)
            transform.position = new Vector3(Random.Range(-9.5f, 9.5f), 6f, 0f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_isDead)
            return;

        if (other.tag == "Player")
        {
            if (_player == null)
                _player = other.GetComponent<Player>();
            if (_player == null)
                _player = GameObject.Find("Player").GetComponent<Player>();


            if (_player != null)
            {
                _player.AddScore();
                _player.Damage();
            }

            Death();
        }

        if (other.tag == "Laser")
        {
            if(_player == null)
                _player = GameObject.Find("Player").GetComponent<Player>();
            _player.AddScore();
            Destroy(other.gameObject);
            Death();
        }
    }

    private void Death()
    {
        _isDead = true;
        _speed = _speed / 3f;
        Animator anim = GetComponent<Animator>();
        AudioSource audioSource = GetComponent<AudioSource>();
        if (anim == null)
            Debug.LogError("Anim == null");

        anim.SetTrigger("OnEnemyDeath");
        audioSource.Play();
        Destroy(this.gameObject, 3f);
    }
}
