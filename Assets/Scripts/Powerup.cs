﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    enum Type
    {
        TripleShot,
        SpeedUp,
        Shield
    };

    [SerializeField]
    private float _speed = 3f;
    [SerializeField]
    private Type _type = Type.TripleShot;
    [SerializeField]
    private AudioClip _powerupSound;


    void Update()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        if (transform.position.y > 7f || transform.position.y < -7f
            || transform.position.x > 11f || transform.position.x < -11f)
            Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if (player != null) {
                switch (_type)
                {
                    case Type.TripleShot:
                        player.TripleShotActive();
                        break;
                    case Type.SpeedUp:
                        player.SpeedUpActive();
                        break;
                    case Type.Shield:
                        player.ShieldActive();
                        break;
                }
                AudioSource.PlayClipAtPoint(_powerupSound, transform.position);
            }

            Destroy(this.gameObject);
        }
    }
}
