﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    [SerializeField]
    private float _speed = 10f;

    void Update()
    {
        transform.Translate(Vector3.up * _speed * Time.deltaTime);

        if (transform.position.y > 7f || transform.position.y < -7f 
            || transform.position.x > 11f || transform.position.x < -11f)
        {
            if (this.transform.parent != null && this.transform.parent.name.StartsWith("Triple_Shot"))
            {
                Destroy(this.transform.parent.gameObject);
            }
            Destroy(this.gameObject);
        }
    }
}
