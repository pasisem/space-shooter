﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class GameManager : MonoBehaviour
{
    private bool _isGameOver = false;
    [SerializeField]
    private GameObject _mobileControls;

    private void Update()
    {
#if UNITY_ANDROID
        if (Input.GetMouseButtonUp(0) && _isGameOver)
#else
        if (Input.GetKeyDown(KeyCode.R) && _isGameOver)
#endif
        {
            SceneManager.LoadScene(1); // Game Scene
        }
    }

    private void Start()
    {
        
    }

    public void GameOver()
    {
        _isGameOver = true;
        _mobileControls.SetActive(false);
    }
}
