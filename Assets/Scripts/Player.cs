﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float _speed = 5f;
    [SerializeField]
    private float _speedMultiplier = 2f;
    [SerializeField]
    private int _lives = 3;
    [SerializeField]
    private float _stopLimitHorizontal = 12f;
    [SerializeField]
    private float _stopLimitVertical = 5f;
    [SerializeField]
    private float _wrapLimitHorizontal = 13f;
    [SerializeField]
    private float _wrapLimitVertical = 6f;
    [SerializeField]
    private bool _wrapWarp = false;
    [SerializeField]
    private GameObject _laserPrefab;
    [SerializeField]
    private GameObject _tripleShotPrefab;
    [SerializeField]
    private GameObject _shield;
    private bool _tripleShotActive = false;
    private bool _speedUpActive = false;
    private bool _shieldActive = false;

    [SerializeField]
    private GameObject _engineFireLeft, _engineFireRight;
    [SerializeField]
    private GameObject _explosionPrefab;
    [SerializeField]
    private float _fireRate = 0.5f;
    private float _canFire = -1f;
    private AudioSource playerAudioSource;
    private AudioClip _laserShotClip;
    private SpawnManager _spawnManager;
    [SerializeField]
    private int _score = 0;
    [SerializeField]
    private UIManager _uiManager;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(0, -3, 0);
        // finding object and it's script
        _spawnManager = GameObject.Find("Spawn_Manager").GetComponent<SpawnManager>();
        if (_uiManager == null)
            GameObject.Find("Canvas").GetComponent<UIManager>();

        playerAudioSource = GetComponent<AudioSource>();

        if (playerAudioSource == null)
            Debug.LogError("Audio Source on Player null!");
        else if (playerAudioSource.clip == null && _laserShotClip != null)
            playerAudioSource.clip = _laserShotClip;
        if (_spawnManager == null)
            Debug.LogError("Failed to find Spawn Manager! (null)");
        if (_uiManager == null)
            Debug.LogError("Failed to find UI Manager! (null)");
    }

    // Update is called once per frame
    void Update()
    {
        CalculateMovement();
        ShootingLaser();
    }

    private void CalculateMovement()
    {
        float horizontalInput;
        float verticalInput;
#if UNITY_ANDROID
        horizontalInput = CrossPlatformInputManager.GetAxis("Horizontal");
        verticalInput = CrossPlatformInputManager.GetAxis("Vertical");
#else
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
#endif
        float randomMovementX = Random.Range(-0.1f, 0.1f);
        float randomMovementY = Random.Range(-0.1f, 0.1f);

        //transform.Translate(new Vector3(randomMovementX, randomMovementY, 0) * Time.deltaTime);

        if (_speedUpActive)
            transform.Translate(new Vector3(horizontalInput + randomMovementX, verticalInput + randomMovementY, 0) * _speed * _speedMultiplier * Time.deltaTime);
        else
            transform.Translate(new Vector3(horizontalInput + randomMovementX, verticalInput + randomMovementY, 0) * _speed * Time.deltaTime);

        if (_wrapWarp)
        {
            // Horizontal Wrap
            if (transform.position.x > _wrapLimitHorizontal)
                transform.position = new Vector3(_wrapLimitHorizontal * -1, transform.position.y, 0f);
            else if (transform.position.x < _wrapLimitHorizontal * -1)
                transform.position = new Vector3(_wrapLimitHorizontal, transform.position.y, 0f);

            // Vertical Wrap
            if (transform.position.y > _wrapLimitVertical)
                transform.position = new Vector3(transform.position.x, _wrapLimitVertical * -1, 0f);
            else if (transform.position.y < _wrapLimitVertical * -1)
                transform.position = new Vector3(transform.position.x, _wrapLimitVertical, 0f);
        }
        else
        {
            // limit the range of movement with Mathf.Clamp and stopLimit variables
            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, _stopLimitHorizontal * -1, _stopLimitHorizontal),
                Mathf.Clamp(transform.position.y, _stopLimitVertical * -1, _stopLimitVertical),
                0f);
        }
    }

    private void ShootingLaser()
    {
#if UNITY_ANDROID
        if (CrossPlatformInputManager.GetButtonDown("Fire") && Time.time > _canFire)
#else
        if (Input.GetButtonDown("Fire1") && Time.time > _canFire)
#endif
        {
            _canFire = Time.time + _fireRate;

            if (_tripleShotActive)
            {
                Instantiate(_tripleShotPrefab, transform.position, Quaternion.identity);
                playerAudioSource.pitch = 1f;
            }
            else
            {
                Instantiate(_laserPrefab, transform.position + new Vector3(0, 1.01f, 0), Quaternion.identity);
                playerAudioSource.pitch = 3f;
            }

            playerAudioSource.Play();
        }
    }

    public void Damage()
    {
        if (_shieldActive)
        {
            _shield.SetActive(false);
            _shieldActive = false;
            return;
        }


        _lives--;

        if(_lives == 2)
        {
            int randomEngineNum = (int) Mathf.Floor(Random.Range(0f, 2f));

            if (randomEngineNum == 0)
                _engineFireLeft.SetActive(true);
            else
                _engineFireRight.SetActive(true);
        } else if (_lives < 2)
        {
            _engineFireLeft.SetActive(true);
            _engineFireRight.SetActive(true);
        }

        _uiManager.UpdateLives(_lives);

        if(_lives < 1)
        {
            _spawnManager.StopSpawning();
            _uiManager.ShowGameOverText();
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            _speed = 0f;
            GetComponent<BoxCollider2D>().enabled = false;
            Destroy(this.gameObject, 1f);
        }
        Debug.Log("Lives: " + _lives);
    }


    public void AddScore()
    {
        AddScore(1);
    }
    public void AddScore(int points)
    {
        _score += points;
        _uiManager.UpdateScore(_score);
    }
    
    public void TripleShotActive()
    {
        _tripleShotActive = true;
        StopCoroutine(TripleShotPowerDownRoutine());
        StartCoroutine(TripleShotPowerDownRoutine());
    }

    public void SpeedUpActive()
    {
        _speedUpActive = true;
        StopCoroutine(SpeedUpPowerDownRoutine());
        StartCoroutine(SpeedUpPowerDownRoutine());
    }

    public void ShieldActive()
    {
        _shieldActive = true;
        _shield.SetActive(true);
    }

    IEnumerator TripleShotPowerDownRoutine()
    {
        yield return new WaitForSeconds(5);
        _tripleShotActive = false;
    }

    IEnumerator SpeedUpPowerDownRoutine()
    {
        yield return new WaitForSeconds(5);
        _speedUpActive = false;
    }
}
