﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _powerups;
    [SerializeField]
    private GameObject _enemyPrefab;
    [SerializeField]
    private GameObject _enemyContainer;
    [SerializeField]
    private float spawnRate = 5f;
    private bool spawning = true;
    [SerializeField]
    private AudioSource _bgMusic;

    public void StartSpawning()
    {
        StartCoroutine(SpawnEnemyRoutine());
        StartCoroutine(SpawnPowerupRoutine());
    }

    public void StopSpawning()
    {
        spawning = false;
    }

    IEnumerator SpawnEnemyRoutine()
    {
        yield return new WaitForSeconds(3f);
        while (spawning)
        {
            GameObject newEnemy = Instantiate(_enemyPrefab, new Vector3(Random.Range(-9.5f, 9.5f), 6f, 0f), Quaternion.identity);
            newEnemy.transform.parent = _enemyContainer.transform;
            yield return new WaitForSeconds(spawnRate);
        }
    }

    IEnumerator SpawnPowerupRoutine()
    {
        yield return new WaitForSeconds(3f);
        _bgMusic.Play();
        while (spawning)
        {
            GameObject newPowerUp;
            int randomNum = (int) Mathf.Floor(Random.Range(0f, 3f));

            newPowerUp = Instantiate(_powerups[randomNum], new Vector3(Random.Range(-9.5f, 9.5f), 6f, 0f), Quaternion.identity);

            yield return new WaitForSeconds(Random.Range(3, 7));
        }
    }
}
