﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField]
    private float _speed = 20f;
    [SerializeField]
    private GameObject _explotionPrefab;
    private SpawnManager _spawnManager;

    private void Start()
    {
        if (_spawnManager == null)
            _spawnManager = GameObject.Find("Spawn_Manager").GetComponent<SpawnManager>();
    }

    void Update()
    {
        transform.Rotate(Vector3.forward * _speed * Time.deltaTime);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Laser")
        {
            Destroy(other.gameObject);
            GameObject explotion = Instantiate(_explotionPrefab, transform.position, Quaternion.identity);
            _spawnManager.StartSpawning();
            GetComponent<CircleCollider2D>().enabled = false;
            Destroy(this.gameObject, 0.25f);
        }
    }
}
